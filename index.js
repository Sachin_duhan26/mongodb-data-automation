const db = require('mongodb');
const random = require('random-name');

const MongoURI = 'mongodb://localhost:27017/usip-prod';
let database;

db.connect(MongoURI, {
    useNewUrlParser: true,
    useFindAndModify: false
}, (err, db) => {
    if (err) throw err;
    database = db.db('usip-prod');
    for (let i = 0; i < 10; i++) {
        officerData();
    }
});

function getRandomNumber(max, min) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function insertInternData() {

    const name = random.first();
    const last = random.last();
    const marks = getRandomNumber(10, 3);
    const roll = getRandomNumber(200, 0);
    const branch = ['MCE', 'ECE', 'COE', 'EE', 'CE', 'MAM', 'BT', 'PCT', 'IT', 'SE'];
    const index = getRandomNumber(9, 0);
    const year = getRandomNumber(9, 5);
    const phone = getRandomNumber(9999999999, 9000000000);

    const data = {
        "domain": [
            "Web development",
            "App Development"
        ],
        "name": `${name} ${last}`,
        "isSelected": false,
        "interview": false,
        "interview_attendence": false,
        "interview_marks": 0,
        "interview_comment": "none",
        "application_subject": "application for USIP august - october 2019",
        "email": `${name}${last}@gmail.com`,
        "marks": marks,
        "rollNo": `2k1${year}/${branch[index]}/${roll}`,
        "branch": `${branch[index]}`,
        "phone": phone,
        "collegeName": "Delhi Technological University",
        "date": Date.now(),
        "__v": 0,
        "exp": "smart and dedicated in all domian! always wokring hard to achieve something new with superb zeal and motivation!",
    }
    insertData(data, 'registers');
}

function officerData() {
    const first = random.first();
    const last = random.last();
    const phone = getRandomNumber(9999999999, 9900000000);
    const branch = ['MCE', 'ECE', 'COE', 'EE', 'CE', 'MAM', 'BT', 'PCT', 'IT', 'SE'];
    const index = getRandomNumber(9, 0);

    const data = {
        "interns": [],
        "name": `${random.first()} ${random.last()}`,
        "phone": phone,
        "email": `${first}${last}@gmail.com`,
        "deptt": `${branch[index]}`,
        "__v": 0
    }

    insertData(data, "officers");
}

function insertData(data, collection) {
    database.collection(collection).insertOne(data).then((result) => {
        console.log('inserted! ');
    }).catch((err) => {
        console.log(err);
    });
}